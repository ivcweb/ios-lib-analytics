# Introduction
This projects provides a library to send analytics on iOS projects to Lead Web's analycis server, made by Christophe Faribault.

# Prerequisites
Although you can build the XCode project and move the created framework to your project, it is recommanded to use [Carthage](https://github.com/Carthage/Carthage).

# Basic usage

## With Carthage
1. Create a file named Cartfile in the root folder of your project
2. Add a line with the path of the library.
git "git@bitbucket.org:lead-web/ios-lib-networking.git"
You can also specify a version number if needed. Please refer to the official documentation of Carthage for more information.
3. Run this command in the root folder of you project
```sh
$ carthage update
```
4. Drag and drop the generated libraries in Carthage/Build in your project Embedded Binaries

## With XCode
1. Build the library in XCode
2. Drag and drop the generated libraries in Carthage/Build in your project Embedded Binaries

# How to use the library?
## Basic setup
The library requires at least one new key in your Info.plist: ***AnalyticsBaseURL***.
This key is used to specify the base URL of the analtyics server.

You should also provide another key if you want to track the user's connection: ***AnalyticsConnectionTrackerURL***.
It provides the base URL for the socket server that tracks the connection drops of the device. This key is not mandatory if you don't want to track connectivity.

## Log in
The library requires credentials to be used.
When the application launches, typically in your AppDelegate, log in to the analytics server
```swift
Analytics.instance.setCredentials(username: "user", password: "pass")
Analytics.instance.login()
```

### Library usage
Every analytics call sould be made with the Analytics instance.

#### Sessions
Calls should be send inside a session. A session can be seen as the use of the application by a single user.
To start a session you must provide a story. A story is the application identifier for the analytics server.
```swift
Analytics.instance.startSession("myStory")
```
To end a session:
```swift
Analytics.instance.endSession()
```

#### Actions / Events
To send events, call the available tag... methods of addAction.
If possible, use the tag methods rather than addAction method to be sure that you are tagging events with naming conventions approved by Christophe Faribault.
The method addAction allows you to tag whatever you want without any check on the passed strings, while tagEvent uses valided names.
[More info here](https://leadweb.atlassian.net/wiki/display/PROJ/3%29+App+with+statistics)

