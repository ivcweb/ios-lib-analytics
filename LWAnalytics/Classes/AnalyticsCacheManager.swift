//
//  AnalyticsCacheManager.swift
//
//  Created by Cedric Kemp on 27/08/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation
import LWNetworking

class AnalyticsCache: JSONConvertible {
    var sessions = [AnalyticsSession]()
    var user: AnalyticsUser?
    
    func toJSON() -> JSONDictionary {
        var json = JSONDictionary()
        json["sessions"] = self.sessions.toJSON()
        json.setValueIfNotNil(value: self.user?.toJSON(), forKey: "user")
        
        return json
    }
    
    static func fromJSON(json: JSONDictionary) -> AnalyticsCache {
        let cache = AnalyticsCache()
        json.assign(&cache.sessions, forKey: "sessions")
        if let dic = json["user"] as? JSONDictionary {
            cache.user = AnalyticsUser.fromJSON(json: dic)
        }
        return cache
    }
}

class AnalyticsCacheManager {
    static let instance = AnalyticsCacheManager()
    
    private var cache: AnalyticsCache?
    private var persistQueue = OperationQueue()
    
    private init() {}
    
    
    //MARK: - Private methods
    
    private static func cacheDirectory() -> String {
        var path = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first! as NSString
        path = path.appendingPathComponent("Analytics") as NSString
        
        return path as String
    }
    
    private static func cacheFilePath() -> String {
        return (self.cacheDirectory() as NSString).appendingPathComponent("Cache")
    }
    
    /**
     Saves the cache into a file
     **/
    private func persistCache(cache: AnalyticsCache) {
        do {
            if !FileManager.default.fileExists(atPath: AnalyticsCacheManager.cacheDirectory()) {
                try FileManager.default.createDirectory(atPath: AnalyticsCacheManager.cacheDirectory(), withIntermediateDirectories: true, attributes: nil)
            }
            let path = AnalyticsCacheManager.cacheFilePath()
            let data = try JSONSerialization.data(withJSONObject: cache.toJSON(), options: .prettyPrinted) as NSData
            try data.write(toFile: path, options: .atomicWrite)
            
            print("Analytics cache peristed at \(path)")
        }
        catch {
            print("Could not cache file")
        }
    }
    
    /**
     Retreive the stored cached analytics
     **/
    
    private func fetchCache() -> AnalyticsCache? {
        let path = AnalyticsCacheManager.cacheFilePath()
        if let data = NSData(contentsOfFile: path) {
            if let json = try? JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions(rawValue: 0)), let dic = json as? JSONDictionary {
                //print("Analytics cache fetched at \(path)")
                return AnalyticsCache.fromJSON(json: dic)
            }
            else {
                print("Invalid JSON in cache file. File will be deleted")
                do {
                    try FileManager.default.removeItem(atPath: path)
                }
                catch {}
            }
        }
        return nil
    }
    
    
    /**
     Saves current cache
     **/
    func persistCache() {
        self.persistQueue.cancelAllOperations()
        
        self.persistQueue.addOperation {
            if let cache = self.cache {
                self.persistCache(cache: cache)
            }
        }
    }
    
    private func getCache(createIfNotFound create: Bool) -> AnalyticsCache? {
        var cache: AnalyticsCache! = self.cache
        if cache == nil {
            cache = self.fetchCache()
            if cache == nil && create {
                cache = AnalyticsCache()
            }
            self.cache = cache
        }
        
        return cache
    }
    
    //MARK: - Public methods
    
    /**
     Adds the sessions to the current cache.
     Call persistCache to save the changes on disk
     **/
    func cacheSessions(sessions: [AnalyticsSession]) {
        self.getCache(createIfNotFound: true)!.sessions.append(contentsOf: sessions)
    }
    
    /**
     Remove all cached sessions. Typically sent when all the cached sessions were sent to the server.
     **/
    func emptyCachedSessions() {
        self.getCache(createIfNotFound: false)?.sessions.removeAll()
    }
    
    func getCachedSessions() -> [AnalyticsSession]? {
        return self.getCache(createIfNotFound: false)?.sessions
    }
    
    /**
     Cache the current user
     Call persistCache to save the changes on disk
     **/
    func cacheUser(user: AnalyticsUser) {
        self.getCache(createIfNotFound: true)!.user = user
    }
    
    func getUser() -> AnalyticsUser? {
        return self.getCache(createIfNotFound: false)?.user
    }
}
