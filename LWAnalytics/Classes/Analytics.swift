//
//  Analytics.swift
//
//  Created by Cedric Kemp on 26/08/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation

public class Analytics {
    private static let baseURL: String = Bundle.main.object(forInfoDictionaryKey: "AnalyticsBaseURL") as! String
    private static let debug: Bool = Bundle.main.object(forInfoDictionaryKey: "AnalyticsDebug") as? Bool ?? false
    private static let connectionStatusTrackerURL = Bundle.main.object(forInfoDictionaryKey: "AnalyticsConnectionTrackerURL") as? String
    
    public static let instance = Analytics()
    
    public enum LoginStatus {
        /// No credentials were provided
        case noCredentailsProvided
        
        /// Credentials provided but not logged and not currently logging in
        case notLogged
        
        /// The user is currently performing a login request
        case loggingIn
        
        /// The user has successfully logged in
        case loggedIn
        
        /// The user did fail logging in (bad credentials, network issue, ...)
        case failed
    }
    
    /// Special code, usually to identify a single person when the device is shared amongst mutliple users.
    open var specialCode: String? {
        didSet {
            if let session = self.session {
                session.specialCode = specialCode
            }
        }
    }
    
    private var username: String?
    private var password: String?
    private(set) var user: AnalyticsUser?
    private var session: AnalyticsSession?
    private var loginDataProvider: AnalyticsLoginDataProvider?
    private var sendSessionDataProvider: AnalyticsSendSessionDataProvider?
    private var connectionStatusTracker: ConnectionStatusTracker?
    
    public var userId: Int? {
        return self.user?.id
    }
    
    
    // MARK: - Private methods
    
    private init() {
        //Try to flush the cached data
        if let user = AnalyticsCacheManager.instance.getUser() {
            self.sendSessions(sessions: [], cachedSessions: AnalyticsCacheManager.instance.getCachedSessions(), user: user)
        }
    }
    
    /**
     Creates a mutable request with the base URL and the given path which is the base for all requests.
     
     - param path: The path of the request (after the base URL). Eg: https://api.server.com/path
     - param httpMethod: HTTP method (GET, POST, PUT, DELETE, ...)
     - returns: The request
     */
    
    private func basicRequest(path: String, httpMethod: String, token: String? = nil) -> URLRequest {
        let url: String = Analytics.baseURL + "/" + path
        var request: URLRequest = URLRequest(url: URL(string: url)!)
        request.httpMethod = httpMethod
        if let token = token {
            request.setValue(token, forHTTPHeaderField: "X-AUTH-TOKEN")
        }
        return request
    }
    
    /**
     Creates a mutable request with the base URL and the given path receiving JSON parameters.
     
     - param path: The path of the request (after the base URL). Eg: https://api.server.com/path
     - param params: The body params to be sent in JSON
     - param session: Current session for authenticated requests
     - returns: The request
     */
    private func basicJSONRequest(path: String, method: String, params: Any?, token: String? = nil) -> URLRequest {
        var request = self.basicRequest(path: path, httpMethod: method, token: token)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var shouldAddContentLength = true
        if let params = params as? [String : AnyObject], params.count == 0 {
            shouldAddContentLength = false
        }
        if let params = params as? [String : AnyObject], params.count == 0 {
            shouldAddContentLength = false
        }
        
        if let params = params, shouldAddContentLength {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions(rawValue: 0))
                request.setValue("\(request.httpBody!.count)", forHTTPHeaderField: "Content-Length")
            }
            catch {
                print("Invalid json body")
            }
        }
        
        return request
    }
    
    
    private static func _apiDateFormatter() -> DateFormatter {
        let formatter = self.createDateFormatter()
        formatter.dateFormat = "yyyy-M-dd HH:mm:ss"
        return formatter
    }
    
    private static func createDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.timeZone = Analytics.timezone
        return formatter
    }
    
    ///  Try to send the sessions to the analtycs sever. If the request did fail, the sessions are persisted to the cache file.
    ///
    /// - Parameters:
    ///   - sessions: Sessions to send and to add in cache in case of failure
    ///   - cachedSessions: Sessions already cached that don't need to be added in the cache
    ///   - user: The user related to the stats
    private func sendSessions(sessions: [AnalyticsSession], cachedSessions: [AnalyticsSession]?, user: AnalyticsUser) {
        self.sendSessionDataProvider?.cancel()
        
        if sessions.count == 0 && cachedSessions?.count == 0 {
            print("Analytics: Nothing to send")
            return
        }
        
        if let token = user.token, let userId = user.id {
            self.sendSessionDataProvider = AnalyticsSendSessionDataProvider()
            //self.sendSessionDataProvider!.debugJSON = true
            self.sendSessionDataProvider!.completion = { (results, error, canceled) in
                self.sendSessionDataProvider?.debugStatus()
                
                if canceled {
                    return
                }
            
                //Success
                if error == nil && self.sendSessionDataProvider?.serverHTTPStatusCode == 200 {
                    AnalyticsCacheManager.instance.emptyCachedSessions()
                    AnalyticsCacheManager.instance.persistCache()
                }
                
                //Cache new sessions if error
                else if sessions.count > 0 {
                    AnalyticsCacheManager.instance.cacheSessions(sessions: sessions)
                    AnalyticsCacheManager.instance.persistCache()
                }
            }
            var allSessions = [AnalyticsSession]()
            allSessions.append(contentsOf: sessions)
            if let cachedSessions = cachedSessions {
                allSessions.append(contentsOf: cachedSessions)
            }
            let request = self.sendSessionsRequest(sessions: allSessions, userId: userId, token: token)
            self.sendSessionDataProvider!.performRequest(request: request)
        }
        else {
            print("No current user token or user id")
        }
    }
    
    //MARK: - Requests
    
    private func loginRequest(username: String, password: String) -> URLRequest {
        var params = [String : Any]()
        params["username"] = username
        params["password"] = password
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            params["version"] = version
        }
        if let version = Bundle.main
            .infoDictionary?["CFBundleVersion"] as? String {
            params["build"] = version
        }
        
        return self.basicJSONRequest(path: "login", method: "POST", params: params)
    }
    
    private func sendSessionsRequest(sessions: [AnalyticsSession], userId: Int, token: String) -> URLRequest {
        var sessionsParam = [Any]()
        for session in sessions {
            sessionsParam.append(session.toJSON())
        }
        let params = ["sessions" : sessionsParam]
        return self.basicJSONRequest(path: "devices/\(userId)/sessions", method: "POST", params: params, token: token)
    }
    
    //MARK: - Public methods
    
    /// The date formatter used for the web services
    public static let apiDateFormatter: DateFormatter = Analytics._apiDateFormatter()
    
    /// The timezone used when sending dates. Default is Montreal timezone (EDT).
    public static let timezone = TimeZone(abbreviation: "EDT")!
    
    public static let badCredentialsStatusCode = 401
    
    /// A session has already started
    public var hasSession: Bool {
        return self.session != nil
    }
    
    /// User is currently logged with the provided credentials
    public var isLogged: Bool {
        return self.user != nil
    }
    
    /// Username and password were set
    public var credentialsProvided: Bool {
        return self.username != nil && self.password != nil
    }
    
    /// Current login status
    public private(set) var loginStatus: LoginStatus = LoginStatus.noCredentailsProvided
    
    /// Status code of the login request. Useful to understand why the app could not connect to the analytics server.
    public private(set) var loginServerStatusCode: Int?
    
    public func setCredentials(username: String, password: String) {
        self.logOut()
        
        self.username = username
        self.password = password
    }
    

    /// Starts an new analytics session.
    /// You need to provide the username and the password to authenticate in case we are not logged in yet.
    ///
    /// - Parameter story: The name you want to give for the context of the session.
    public func startSession(story: String) {
        if self.session != nil {
            self.endSession()
        }
        
        //Login if needed
        if self.user == nil {
            self.login { (success) in
                if let user = self.user, user.ip != nil && user.token != nil {
                    self.startSession(story: story)
                }
            }
        }
        
        //Create new session
        else {
            self.session = AnalyticsSession()
            self.session!.startDate = Date()
            self.session?.ip = self.user!.ip
            self.session?.story = story
            self.session?.specialCode = self.specialCode
        }
    }
    
    /**
     Ends the session and send all the analytics data to the server.
     If the send request did fail, the analytics are cached and sent later.
     **/
    public func endSession(){
        if let session = self.session, let user = self.user {
            session.endDate = Date()
            
            //Send current session and the remaining cached sessions
            self.sendSessions(sessions: [session], cachedSessions: AnalyticsCacheManager.instance.getCachedSessions(), user: user)
            self.session = nil
        }
        else {
            print("No current session to send or no current user")
        }
        self.session = nil
    }
    
    /**
     Add an event of type action
     - parameter actionType: The type of action (interactive action, video,...)
     - parameter eventType: The type of event (page view, click, swipe, pause,...)
     - parameter extraInfo: More details about the action
     **/
    public func addAction(actionType: String, eventType: String, extraInfo: String?) {
        if let session = self.session {
            let action = AnalyticsSessionAction()
            action.name = actionType
            action.value = eventType
            action.extra = extraInfo
            action.date = Date()
            
            session.actions.append(action)
        }
        else {
            print("No current session")
        }
    }
    
    
    /// Logs in with the given credentials at fetch the token retreive
    ///
    /// - Parameter complete: The completion handler. First parameter is success boolean.
    public func login(complete: ((Bool) -> Void)? = nil){
        self.logOut()
        
        self.loginDataProvider?.cancel()
        
        if let username = self.username, let password = self.password {
            self.loginStatus = .loggingIn
            self.loginDataProvider = AnalyticsLoginDataProvider()
            self.loginDataProvider!.completion = { (user, error, canceled) in
                if canceled {
                    return
                }
                
                self.loginServerStatusCode = self.loginDataProvider?.serverHTTPStatusCode
                
                if let message = self.loginDataProvider?.errorMessage,
                    self.loginDataProvider?.serverHTTPStatusCode != AnalyticsLoginDataProvider.kSuccessHTTPCode {
                    print("Analytics error: \(message)")
                }
                
                if let user = user {
                    //Cache user
                    AnalyticsCacheManager.instance.cacheUser(user: user)
                    AnalyticsCacheManager.instance.persistCache()
                    self.user = user
                    
                    // Setup connection status tracker
                    if let sUrl = Analytics.connectionStatusTrackerURL, let url = URL(string: sUrl) {
                        self.connectionStatusTracker = ConnectionStatusTracker(socketURL: url, debug: Analytics.debug)
                         self.connectionStatusTracker!.start()
                    }
                    else {
                        print("Socket URL for connection status tracker not provided (AnalyticsConnectionTrackerURL) in Info.plist. The connection status will not be tracked.")
                    }
                    self.loginStatus = .loggedIn
                    
                    complete?(true)
                }
                    
                else {
                    self.loginDataProvider?.debugStatus()
                    if let user = AnalyticsCacheManager.instance.getUser(), user.username == self.username {
                        print("Analytics did fetch cached user")
                        self.user = user
                    }
                    self.loginStatus = .failed
                    complete?(false)
                }
            }
            self.loginDataProvider!.performRequest(request: self.loginRequest(username: username, password: password))
        }
        else {
            print("Username and password must be set to use analytics")
            complete?(false)
        }
    }
    
    /// Log out from the analytrics framework
    public func logOut(){
        if self.session != nil {
            self.endSession()
        }
        self.connectionStatusTracker?.stop()
        self.user = nil
        self.loginStatus = .notLogged
        self.loginServerStatusCode = nil
    }
}
