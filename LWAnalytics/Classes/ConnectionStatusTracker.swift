//
//  ConnectionStatusTracker.swift
//
//  Created by Cedric Kemp on 14/10/16.
//  Copyright © 2016 Dermwall. All rights reserved.
//

import Foundation
import SocketIO
import LWNetworking

class ConnectionStatusTracker {
    private var socketManager: SocketManager
    private var socket: SocketIOClient {
        get {
            return self.socketManager.defaultSocket
        }
    }
    
    /// Will perform a ping on the socket every pingInterval seconds
    var pingInterval: TimeInterval = 5
    private(set) var clientId: String?
    private var pingTimer: Timer?
    private var debug: Bool = false
    
    init(socketURL: URL, debug: Bool = false) {
        self.debug = debug
        self.socketManager = SocketManager(socketURL: socketURL, config: [.log(debug)])
        
        self.socket.on("connect") {data, ack in
            print("Socket connected on \(socketURL.absoluteString)")
            self.pingTimer?.invalidate()
            self.clientId = nil
        }
        
        socket.on("socket-client-id") {data, ack in
            if let data = data.first as? JSONDictionary {
                self.clientId = data["id"] as? String
                
                //Start pinging the server
                self.pingTimer?.invalidate()
                self.pingTimer = Timer.scheduledTimer(timeInterval: self.pingInterval, target: self, selector: #selector(self.pingSocketServer), userInfo: nil, repeats: true)
            }
        }

        socket.on("disconnect") {data, ack in
            print("Socket disconnection succeeded")
            self.clientId = nil
            self.pingTimer?.invalidate()
        }
    }
    
    deinit {
        self.socket.disconnect()
    }
    
    /// Starts the socket
    func start(){
        if self.socket.status == SocketIOStatus.disconnected || self.socket.status == SocketIOStatus.notConnected {
            self.socket.connect()
        }
    }
    
    func stop() {
        self.socket.disconnect()
    }
    
    @objc private func pingSocketServer(){
        if let id = Analytics.instance.user?.id, let clientId = self.clientId {
            if self.debug {
                print("Ping connection status socket")
            }
            let info: [String : Any] = ["device" : id, "socket" : clientId]
            let json = try! JSONSerialization.data(withJSONObject: info, options: JSONSerialization.WritingOptions(rawValue: 0))
            let string = String(data: json, encoding: String.Encoding.utf8)!
            
            if self.debug {
                print(string)
            }
            
            self.socket.emit("socket:ping", info)
        }
    }
}
