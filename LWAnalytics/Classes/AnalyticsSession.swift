//
//  AnalyticsSession.swift
//
//  Created by Cedric Kemp on 26/08/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation
import LWNetworking

class AnalyticsSessionAction: JSONConvertible {
    var name: String?
    var value: String?
    var extra: String?
    var date: Date?
    
    func toJSON() -> JSONDictionary {
        var actionJSON = JSONDictionary()
        actionJSON.setValueIfNotNil(value: self.name, forKey: "name")
        actionJSON.setValueIfNotNil(value: self.value, forKey: "value")
        actionJSON.setValueIfNotNil(value: self.extra, forKey: "extra")
        if let date = self.date {
            actionJSON["time"] = Analytics.apiDateFormatter.string(from: date as Date)
        }
        
        return actionJSON
    }
    
    static func fromJSON(json: JSONDictionary) -> AnalyticsSessionAction {
        let action = AnalyticsSessionAction()
        json.assign(&action.name, forKey: "name")
        json.assign(&action.value, forKey: "value")
        json.assign(&action.extra, forKey: "extra")
        json.assign(&action.date, forKey: "time", dateFormatter: Analytics.apiDateFormatter)
        return action
    }
}

class AnalyticsSession: JSONConvertible {
    var story: String?
    var startDate: Date?
    var endDate: Date?
    var ip: String?
    var actions: [AnalyticsSessionAction] = []
    var specialCode: String?
    
    func toJSON() -> JSONDictionary {
        var json = JSONDictionary()
        json.setValueIfNotNil(value: self.story, forKey: "story")
        json.setValueIfNotNil(value: self.ip, forKey: "ip")
        json.setValueIfNotNil(value:
            self.specialCode, forKey: "code")
        if let date = self.startDate {
            json["start_time"] = Analytics.apiDateFormatter.string(from: date as Date)
        }
        if let date = self.endDate {
            json["end_time"] = Analytics.apiDateFormatter.string(from: date as Date)
        }
        
        json["actions"] = self.actions.toJSON()
        
        return json
    }
    
    static func fromJSON(json: JSONDictionary) -> AnalyticsSession {
        let session = AnalyticsSession()
        json.assign(&session.story, forKey: "story")
        json.assign(&session.ip, forKey: "ip")
        json.assign(&session.specialCode, forKey: "code")
        json.assign(&session.startDate, forKey: "start_time", dateFormatter: Analytics.apiDateFormatter)
        json.assign(&session.startDate, forKey: "end_time", dateFormatter: Analytics.apiDateFormatter)
        json.assign(&session.actions, forKey: "actions")
        return session
    }
}

