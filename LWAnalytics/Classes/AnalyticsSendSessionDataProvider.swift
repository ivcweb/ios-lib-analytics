//
//  AnalyticsSendSessionDataProvider.swift
//
//  Created by Cedric Kemp on 26/08/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation
import LWNetworking

class AnalyticsSendSessionDataProvider: JSONDataProvider<Void> {
    static let kSuccessHTTPCode = 200
}
