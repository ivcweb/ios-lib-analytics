//
//  AnalyticsLoginDataProvider.swift
//
//  Created by Cedric Kemp on 26/08/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation
import LWNetworking

class AnalyticsLoginDataProvider: JSONDataProvider<AnalyticsUser> {
    static let kSuccessHTTPCode = 200
    
    private(set) var errorMessage: String?
    
    override func parseResult(json: JSONDictionary) -> AnalyticsUser? {
        if let profile = json["profile"] as? JSONDictionary {
            let user = AnalyticsUser.fromJSON(json: profile)
            user.ip = json["ip"] as? String
            return user
        }
        self.errorMessage = json["message"] as? String
        return nil
    }
}
