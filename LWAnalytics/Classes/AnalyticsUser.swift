//
//  AnalyticsUser.swift
//
//  Created by Cedric Kemp on 26/08/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation
import LWNetworking

class AnalyticsUser: JSONConvertible {
    var id: Int?
    var username: String?
    var ip: String?
    var token: String?
    
    func toJSON() -> JSONDictionary {
        var json = JSONDictionary()
        
        json.setValueIfNotNil(value: self.username, forKey: "username")
        json.setValueIfNotNil(value: self.id, forKey: "id")
        json.setValueIfNotNil(value: self.token, forKey: "api_key")
        json.setValueIfNotNil(value: self.ip, forKey: "ip")
        return json
    }
    
    static func fromJSON(json: JSONDictionary) -> AnalyticsUser {
        let object = AnalyticsUser()
        object.id = json["id"] as? Int
        json.assign(&object.ip, forKey: "ip")
        json.assign(&object.username, forKey: "username")
        json.assign(&object.token, forKey: "api_key")
        return object
    }
}
