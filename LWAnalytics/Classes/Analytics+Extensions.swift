//
//  Analytics+Extensions.swift
//
//  Created by Cedric Kemp on 13/09/16.
//  Copyright © 2017 Leadweb. All rights reserved.
//

import Foundation

public protocol EventProtocol: RawRepresentable {
    /// The name of the event
    static var name: String { get }
}

public struct AnalyticsEvents {
    private init() {}
    
    /// Separator to use between extra info words from a same element
    public static let extraInfoWordSeparator = "-"
    /// Separator to use between extra info words from elements
    public static let extraInfoElementSeparator = "_"
    
    /// Interactive event (click, gesture,...)
    public enum Action: String, EventProtocol {
        public static let name = "action"
        
        case click      = "click"
        case swipe      = "swipe"
        case pan        = "pan"
        case scratch    = "scratch"
        case spin       = "spin"
        
        /// Code, password,... entered
        case code       = "code"
        case complete   = "complete"
        
        /// Quit before the session ended (= quit before the expected end of the process)
        case quit       = "quit"
        
        public struct ExtraInfo {
            public static let button   = "button"
            public static let left     = "left"
            public static let right    = "right"
            public static let up       = "up"
            public static let down     = "down"
            
            /// Creates extra info string from a button
            ///
            /// - Parameter button: The name of the button
            /// - Returns: The sanatized extra info for a button
            public static func from(button: String) -> String {
                return AnalyticsEvents.sanatizeExtraInfo(string: "\(ExtraInfo.button)-\(button)")
            }
        }
    }
    
    /// Page (~= view controller) related event
    public enum Page: String, EventProtocol {
        public static let name = "page"
        
        case view = "view"
    }
    
    /// Video related event
    public enum Video: String, EventProtocol {
        public static let name = "video"
        
        case play       = "play"
        case seek       = "seek"
        case complete   = "complete"
    }
    
    /// Contest related event
    public enum Contest: String, EventProtocol {
        public static let name = "contest"
        
        case enter          = "enter"
        case complete       = "complete"
        
        public struct ExtraInfo {
            public static let winner   = "winner"
            public static let loser    = "loser"
        }
    }
    
    /// Questionnaire related event
    public enum Questionnaire: String, EventProtocol {
        public static let name = "questionnaire"
        
        case start      = "start"
        case quit       = "quit"
        case complete   = "complete"
    }
    
    /// Communication between application and 3rd party
    public enum Send: String, EventProtocol {
        public static let name = "send"
        
        case email = "email"
    }
    
    /// Generic subscription
    public enum Subscribe: String, EventProtocol {
        public static let name = "subscribe"
        
        case email = "email"
    }
    
    /// Scan (QR code for example)
    public enum Scan: String, EventProtocol {
        public static let name = "scan"
        
        case result = "result"
        
        public struct ExtraInfo {
            public static let success    = "success"
            public static let failure    = "failure"
        }
    }
    
    // MARK: Methods
    
    /// Sanatizes an exatra info string so that it has a valid format
    ///
    /// - Parameter string: The string to sanatize
    /// - Returns: The sanatized string
    public static func sanatizeExtraInfo(string: String) -> String {
        return string.replacingOccurrences(of: "_", with: "-")
            .replacingOccurrences(of: " ", with: "-")
            .lowercased()
    }
}


extension Analytics {
    /**
     Add an event of type action
     - parameter actionType: The type of action (interactive action, video,...)
     - parameter eventType: The type of event (page view, click, swipe, pause,...)
     - parameter extraInfo: More details about the action
     **/
    public func tag<T: EventProtocol>(event: T, extraInfo: String?) where T.RawValue == String {
        self.addAction(actionType: T.name, eventType: event.rawValue, extraInfo: extraInfo)
    }
    
    public func tag<T: EventProtocol>(event: T, extraInfo: [String], pageIdentifier: String? = nil) where T.RawValue == String {
        self.addAction(actionType: T.name, eventType: event.rawValue, pageIdentifier: pageIdentifier, extraInfo: extraInfo)
    }
    
    public func addAction(actionType: String, eventType: String, pageIdentifier: String?, extraInfo: [String]? = nil) {
        var extraInfoComponents = [String]()
        if let identifier = pageIdentifier, identifier.count > 0 {
            extraInfoComponents.append(identifier)
        }
        if let extraInfo = extraInfo {
            extraInfoComponents.append(contentsOf: extraInfo)
        }
        
        //Sanatize extra info
        for i in 0 ..< extraInfoComponents.count {
            extraInfoComponents[i] = AnalyticsEvents.sanatizeExtraInfo(string: extraInfoComponents[i])
        }
        
        Analytics.instance.addAction(actionType: actionType, eventType: eventType, extraInfo: extraInfoComponents.joined(separator: AnalyticsEvents.extraInfoElementSeparator))
    }
    
    /// Check if the library is ready to preform analytics and send a debug message of the current status.
    ///
    /// - Returns: A tuple containing a boolean (true if the library is ready and the status message).
    public func debugReadyStatus() -> (Bool, String) {
        switch self.loginStatus {
        case .noCredentailsProvided:
            return (false, "No analytics credentials set in app settings. The application will not send any stats.")
        case .notLogged:
            return (false, "Credentials were provided to the analytics library but no login was called.")
        case .loggingIn:
            return (false, "The analytics library is currently logging in.")
        case .loggedIn:
            return (true, "The analytics library is logged in and can perform analytics.")
        case .failed:
            if self.loginServerStatusCode == Analytics.badCredentialsStatusCode {
                return (false, "Could not connect to analytics server. Bad credentials provided (\(Analytics.badCredentialsStatusCode)).")
            }
            return (false, "No analytics could not log in.")
        }
    }
}
