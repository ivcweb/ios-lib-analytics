//
//  Array+Extensions.swift
//  Pods
//
//  Created by Cedric Kemp on 17-05-05.
//
//

import Foundation
import LWNetworking

extension Array where Element: JSONConvertible {
    func toJSON() -> [JSONDictionary] {
        var items = [JSONDictionary]()
        for item in self {
            items.append(item.toJSON())
        }
        return items
    }
}
