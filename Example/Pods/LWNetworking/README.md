# LWNetworking

[![CI Status](http://img.shields.io/travis/jaeggerr/LWNetworking.svg?style=flat)](https://travis-ci.org/jaeggerr/LWNetworking)
[![Version](https://img.shields.io/cocoapods/v/LWNetworking.svg?style=flat)](http://cocoapods.org/pods/LWNetworking)
[![License](https://img.shields.io/cocoapods/l/LWNetworking.svg?style=flat)](http://cocoapods.org/pods/LWNetworking)
[![Platform](https://img.shields.io/cocoapods/p/LWNetworking.svg?style=flat)](http://cocoapods.org/pods/LWNetworking)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LWNetworking is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LWNetworking'
```

## Author

jaeggerr, jaeggerr

## License

LWNetworking is available under the MIT license. See the LICENSE file for more info.
