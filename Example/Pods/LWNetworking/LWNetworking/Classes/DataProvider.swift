//
//  DataProvider.swift
//  Lead Web
//
//  Created by Cedric Kemp on 17/08/16.
//  Copyright © 2016 Lead Web. All rights reserved.
//

import Foundation

public enum DataProviderStatus {
    case idle
    case performingRequest
    case parsingResult
    case finished
}

public protocol DataProviderProtocol : class {
    @discardableResult func performRequest(request: URLRequest) -> Bool
    func debugStatus(instanceName: String?)
    func cancel()
    func destroy()
    
    var status: DataProviderStatus { get }
    var isBusy: Bool { get }
    var isCanceled: Bool { get }
    var error: Error? { get }
}

public let DataProviderStatusDidChangeNotification = "DataProviderStatusDidChangeNotification"

open class DataProvider<T> : DataProviderProtocol {
    /// Completion handler. Result - Error - Canceled
    public typealias DataProviderCompletion = (T?, Error?, Bool) -> Void
    
    /// Completion handler when the request did finish (success, cancelled or error).
    public var completion: DataProviderCompletion?
    

    /// Operation queue on which the completion block will be called.
    public var completionQueue: OperationQueue = OperationQueue.main
    
    /// The URL session used for performing the requests
    public var urlSession = URLSession.shared
    
    fileprivate(set) public var status: DataProviderStatus = .idle {
        didSet {
            NotificationCenter.default.post(name: Notification.Name(rawValue: DataProviderStatusDidChangeNotification), object: self, userInfo: nil)
        }
    }
    
    public var isBusy: Bool {
        get {
            return self.status != .idle && self.status != .finished
        }
    }
    
    private(set) public var isCanceled: Bool = false
    
    //Completion variables
    fileprivate(set) public var result: T?
    /// Error returned by the server
    fileprivate(set) public var error: Error?
    /// HTTP status code returned by the server
    fileprivate(set) public var serverHTTPStatusCode: Int?
    /// The currently performed request
    fileprivate(set) public var request: URLRequest?
    
    fileprivate var dataTask: URLSessionDataTask?
    
    public init() {
    }
    
    /// Performs the given requests and parses the result with the class implemetation
    /// A previous request cannot have been previously performed with this object.
    ///
    /// - Parameter request: The request to perform
    /// - Returns: True if the request can be performed, otherwise false.
    @discardableResult public func performRequest(request: URLRequest) -> Bool {
        if self.status != .idle {
            print("Request performing or finished. Please create a new object to perform an new request.")
            return false
        }
        
        self.status = .performingRequest
        self.request = request
        self.dataTask = self.urlSession.dataTask(with: request, completionHandler: { (data, response, error) in
            if let data = data, let httpResponse = response as? HTTPURLResponse {
                self.serverHTTPStatusCode = httpResponse.statusCode
                
                // Parse the result
                self.status = .parsingResult
                self.result = self.parseResult(data: data)
            }

            self.error = error as NSError?
            
            // Additional code before completion
            self.prepareBeforeCompletion()
            
            self.performCompletion()
        }) 
        self.dataTask!.resume()
        return true
    }
    
    
    /// Cancels the request
    public func cancel() {
        self.dataTask?.cancel()
        
        self.result = nil
        self.error = nil
        self.serverHTTPStatusCode = nil
        self.isCanceled = true
        self.status = .finished
    }
    
    /// Cancels the request and don't call any completion block
    public func destroy() {
        self.completion = nil
        self.cancel()
    }
    
    /// For debugging purposes. Prints info about the current status of the data provider.
    ///
    /// - Parameter instanceName: The way the current instance will be named. If not set, the class name is used.
    public func debugStatus(instanceName: String? = nil){
        let _instanceName = instanceName ?? "\(type(of: self))"
        var message = ""
        
        switch self.status {
        case .idle:
            message = "\(_instanceName) is idle"
        case .performingRequest:
            message = "\(_instanceName) is performing request"
        case .parsingResult:
            message = "\(_instanceName) is parsing result"
        case .finished:
            message = "\(_instanceName) is complete"
            if let serverStatusCode = self.serverHTTPStatusCode {
                message +=  " (\(serverStatusCode))"
            }
            if let error = self.error {
                message += " with error: \(error.localizedDescription). "
            }
            else {
                message += ". "
            }
            
            if T.self != Void.self {
                if self.result == nil {
                    message += "No results found."
                }
                else if let result = self.result as? [Any] {
                    message += "Server returned \(result.count) results."
                }
                else if let result = self.result as? NSArray {
                    message += "Server returned \(result.count) results."
                }
                else {
                    message += "Results were returned by the server"
                }
            }
        }
        
        print(message)
    }
    
    /// Parses the result returned by the server
    open func parseResult(data: Data) -> T? {
        return nil
    }
    
    /// Implement here additional code before completion handler is called
    open func prepareBeforeCompletion() {
    }
    
    /// Should only be called by subclasses
    open func performCompletion() {
        // Notify the completion on the specified thread (or main thread if unspecified)
        self.completionQueue.addOperation({
            self.status = .finished
            if self.isCanceled {
                self.result = nil
            }
            self.completion?(self.result, self.error, self.isCanceled)
        })
        self.dataTask = nil
    }
}
