//
//  JSONParser.swift
//  Leadweb
//
//  Created by Cedric Kemp on 2/09/16.
//  Copyright © 2016 Leadweb. All rights reserved.
//

import Foundation

open class JSONParser<T: ParserOutput>: Parser<JSONDictionary, JSONArray, T> {
}
