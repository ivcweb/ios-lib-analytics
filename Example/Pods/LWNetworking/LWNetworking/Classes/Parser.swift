//
//  Parser.swift
//  Leadweb
//
//  Created by Cedric Kemp on 2/09/16.
//  Copyright © 2016 Leadweb. All rights reserved.
//

import Foundation

public protocol ParserOutput: AnyObject {
    init()
}


///  Parses a relam object that will be cached and replace existing object with same primary key
///  Generics types are:
///    -I: The class of the input that will be parsed for a single item (= O)
///    -L: The class of the list of inputs (= list of O) that will be parsed
///    - O: The type of a single item
open class Parser<I: Any, L: Sequence, O: ParserOutput> {
    public init() {
    }
    
    /// Parse the associated object type
    ///
    /// - Parameters:
    ///   - input: The input to parse
    /// - Returns: The parsed object
    public func parse(input: I) -> O? {
        let object = O()
        self.parse(input: input, object: object)
        return object
    }
    
    
    /// Update an existing object with the info parsed from the input
    ///
    /// - Parameters:
    ///   - input: The input to parse
    ///   - object: The object to update
    open func parse(input: I, object: O) {
    }
    
    /// Parse a collection of objects from the associated type
    ///
    /// - Parameters:
    ///   - list: The list of inputs to parse
    public func parse(list: L) -> [O] {
        var items: [O] = []
        for listItem in list {
            if let unwrappedItem = listItem as? I, let parsedItem = self.parse(input: unwrappedItem) {
                items.append(parsedItem)
            }
        }
        return items
    }
}
