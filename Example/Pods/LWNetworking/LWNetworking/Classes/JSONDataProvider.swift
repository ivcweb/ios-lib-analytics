//
//  JSONDataProvider.swift
//  Lead Web
//
//  Created by Cedric Kemp on 17/08/16.
//  Copyright © 2016 Lead Web. All rights reserved.
//

import Foundation

open class JSONDataProvider<T>: DataProvider<T> {
    public enum ResultType {
        case dictionary
        case array
    }

    /// If set to yes, the json result will be printed in the console
    public var debugJSON: Bool = false
    fileprivate(set) var jsonResponse: String?
    
    /// The expected result type returned from the server
    open func resultType() -> ResultType {
        return .dictionary
    }
    
    // MARK: - Private methods
    
    /// Try to return a json dictionary from a server response
    fileprivate func toJSONObject(data: Data) -> AnyObject? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0))
            return json as AnyObject?
        }
        catch {
            print("Invalid json sent by the server")
        }
        return nil
    }
    
    //MARK: - Overriden methods
    
    override public func cancel() {
        super.cancel()
        
        self.jsonResponse = nil
    }
    
    override open func parseResult(data: Data) -> T? {
        if self.debugJSON {
            self.jsonResponse = String(data: data, encoding: String.Encoding.utf8)
        }
        
        if let jsonObject = self.toJSONObject(data: data) {
            if self.debugJSON {
                print(jsonObject)
            }
            
            var result: T? = nil
            
            //Pare the result or results, depending on what the JSON returns
            let resultType = self.resultType()
            if resultType == .dictionary {
                if let resultDic = jsonObject as? JSONDictionary {
                    result = self.parseResult(json: resultDic)
                }
            }
            else if resultType == .array {
                if let resultsArray = jsonObject as? JSONArray {
                    result = self.parseResults(json: resultsArray)
                }
            }
            
            return result
        }
        return nil
    }
    
    /// Parse the JSON results
    ///
    /// - Parameter json: The results as a JSON dictionary
    /// - Returns: The parsed result
    open func parseResult(json: JSONDictionary) -> T? {
        return nil
    }
    
    /// Parse the JSON results
    ///
    /// - Parameter json: The results as a JSON array
    /// - Returns: The parsed result
    open func parseResults(json: JSONArray) -> T? {
        return nil
    }
}
