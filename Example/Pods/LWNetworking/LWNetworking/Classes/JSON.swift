//
//  JSON.swift
//  Pods
//
//  Created by Cedric Kemp on 17-05-02.
//
//

import Foundation

public typealias JSONDictionary = [String : Any]
public typealias JSONArray = [Any]

public protocol JSONConvertible {
    associatedtype U
    func toJSON() -> JSONDictionary
    static func fromJSON(json: JSONDictionary) -> U
}
