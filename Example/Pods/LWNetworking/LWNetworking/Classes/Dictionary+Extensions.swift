//
//  Dictionary+Extensions.swift
//  Pods
//
//  Created by Cedric Kemp on 17-05-02.
//
//

import Foundation

public func += <K, V> ( left: inout [K:V], right: [K:V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

extension Dictionary {
    public mutating func setValueIfNotNil(value: Value?, forKey key: Key) {
        if let value = value {
            self[key] = value
        }
    }
    
    public mutating func setValueIfNotNil(value: Value?, forKey key: Key?) {
        if let key = key, let value = value {
            self[key] = value
        }
    }
}

extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    public func assign(_ property: inout String?, forKey key: Key){
        if let value = self[key] as? String {
            property = value
        }
    }
    
    public func assign(_ property: inout Bool?, forKey key: Key){
        if let value = self[key] {
            if value is Bool {
                property = (value as! Bool)
            }
            else if value is Int {
                property = ((value as! Int) == 0) ? false : true
            }
        }
    }
    
    public func assign(_ property: inout Bool, forKey key: Key){
        if let value = self[key] {
            if value is Bool {
                property = (value as! Bool)
            }
            if value is Int {
                property = ((value as! Int) == 0) ? false : true
            }
        }
    }
    
    public func assign(_ property: inout Int, forKey key: Key) {
        if let value = self[key] {
            if value is Int {
                property = (value as! Int)
            }
            if let value = value as? String, let intValue = Int(value) {
                property = intValue
            }
        }
    }
    
    public func assign(_ property: inout Int?, forKey key: Key) {
        if let value = self[key] {
            if value is Int {
                property = (value as! Int)
            }
            if let value = value as? String, let intValue = Int(value) {
                property = intValue
            }
        }
    }
    
    public func assign(_ property: inout Double, forKey key: Key) {
        if let value = self[key] {
            if value is Int {
                property = (value as! Double)
            }
            if let value = value as? String, let doubleValue = Double(value) {
                property = doubleValue
            }
        }
    }
    
    public func assign(_ property: inout Double?, forKey key: Key) {
        if let value = self[key] {
            if value is Int {
                property = (value as! Double)
            }
            if let value = value as? String, let doubleValue = Double(value) {
                property = doubleValue
            }
        }
    }
    
    public func assign(_ property: inout Float, forKey key: Key) {
        if let value = self[key] {
            if value is Int {
                property = (value as! Float)
            }
            if let value = value as? String, let floatValue = Float(value) {
                property = floatValue
            }
        }
    }
    
    public func assign(_ property: inout Float?, forKey key: Key) {
        if let value = self[key] {
            if value is Int {
                property = (value as! Float)
            }
            if let value = value as? String, let floatValue = Float(value) {
                property = floatValue
            }
        }
    }
    
    public func assign(_ property: inout Date?, forKey key: Key, dateFormatter: DateFormatter){
        if let stringDate = self[key] as? String {
            var date: Date?
            date = dateFormatter.date(from: stringDate)
            
            if let date = date {
                property = date
            }
        }
    }
    
    public func assign<T: JSONConvertible>(_ property: inout [T], forKey key: Key){
        if let jsonItems = self[key] as? [JSONDictionary] {
            var items = [T]()
            for jsonItem in jsonItems {
                let item = T.fromJSON(json: jsonItem) as! T
                items.append(item)
            }
            property = items
        }
    }
}
