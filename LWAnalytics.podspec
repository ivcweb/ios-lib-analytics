#
# Be sure to run `pod lib lint LWAnalytics.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LWAnalytics'
  s.version          = '1.0.1'
  s.summary          = 'Library for Leaweb analytics'
  s.homepage         = 'https://bitbucket.org/lead-web/ios-lib-analytics'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Cedric Kemp' => 'cedric.kemp@icloud.com' }
  s.source           = { :git => 'git@bitbucket.org:ivcweb/ios-lib-analytics.git', :tag => '1.0.1' }
  s.ios.deployment_target = '8.0'
  s.source_files = 'LWAnalytics/Classes/**/*'
  s.dependency 'Socket.IO-Client-Swift', '~> 13.0'
  s.dependency 'LWNetworking', '~> 1.0.0'

end
